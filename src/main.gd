extends Node2D

var time = 0.0
var time_default = 3.0

var game_time = 0.0

onready var balloons = preload("res://balloon.tscn")
onready var label = get_node("Label")

func _process(delta):
	update_score()
	game_time += delta
	time -= delta
	if time <= 0:
		time = time_default
		time_default -= 0.1
		if time_default < .5:
			time_default = .5
		create_balloon()

func update_score():
	label.text = "Score: " + str(Global.score)
	label.text += "\nMissed: " + str(Global.missed)

func create_balloon():
	var size = get_viewport().size
	var x = rand_range(64,size.x-64)
	var y = size.y + 128
	var balloon = balloons.instance()
	balloon.position = Vector2(x,y)
	balloon.speed = rand_range(game_time + 50,game_time+300)
	add_child(balloon)	
